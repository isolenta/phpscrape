<?php
if (in_array('curl', get_loaded_extensions()) == false)
    die ('CURL is not supported');

if (in_array('tidy', get_loaded_extensions()) == false)
    die ('TIDY is not supported');

require(__DIR__ . "/phpscrape.inc.php");

date_default_timezone_set("UTC");
setlocale(LC_ALL,'ru_RU.UTF8');

ps_set_options(
    array(
        "user_agent" => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1541.0 Safari/537.36",
        "use_proxy" => true,
        "proxy_url" => "10.222.19.1:3128",
        "proxy_user" => "shmigirilov",
        "proxy_pass" => "47304730",
        "use_tidy" => false,
        "force_utf8" => true,
    )
);

$base_url = "http://www.woman.ru";
$search_url = "http://www.woman.ru/search/?q={KEYWORD}&where=articles&control_charset=%CA%EE%ED%F2%F0%EE%EB%FC";
$result_url = "http://www.woman.ru/search/articles/{PAGE}/?q={KEYWORD}&sort=&category=&control_charset=%CA%EE%ED%F2%F0%EE%EB%FC";

$keywords = array(
	"�������",
);

// part1: collect articles links
foreach ($keywords as $keyword)
{
	$url = str_replace("{KEYWORD}", urlencode($keyword), $search_url);

	$first_search_page = ps_html_repair( ps_url_get($url) );

	// get search result pages count
	$pages = array();
	ps_query(       
    	$first_search_page,
	    "//*[@id=\"content\"]/div[1]/div[2]/ul[3]/li[last()-1]/a",
	    $pages,
	    array()
	);

	$pcount = $pages[0]["value"];
	if ($pcount > 5) $pcount = 5;

	$links = array();

	// iterate over all pages
	for ($page = 1; $page <= $pcount; $page++)
	{
		// fill list of article links
		$purl = str_replace("{PAGE}", $page, $result_url);
		$purl = str_replace("{KEYWORD}", urlencode($keyword), $purl);

		$pcontent = ps_html_repair( ps_url_get($purl) );

		$plinks = array();
		ps_query_blocks(
			$pcontent,
			$plinks,
			array("root" => "//*[@id=\"content\"]/div[1]/div[2]/ul[5]/li"),
			array(
				"title" => "a",
				"href" => "a/@href",
	        ),
	        false
		);

///		print_r ($plinks);
		foreach($plinks as $plink)
			if ($plink["href"] != "")
				$links[] = $plink["href"];

	} // for pages

	file_put_contents("links_" . $keyword . ".txt", implode("\n", $links));

} // foreach keyword

// part 2: search related article

///$links = file('links.txt', FILE_IGNORE_NEW_LINES);
$win_links = array();

foreach($links as $link)
{
	$article = ps_html_repair( ps_url_get($base_url . $link) );
	
	$iframes = array();
	ps_query(       
    	$article,
	    "//*[@id=\"content\"]/div[1]/div[2]/div[2]/iframe/@src",
	    $iframes,
	    array()
	);

	foreach ($iframes as $iframe)
	{
		if (strpos($iframe["value"], "markerId"))
			$win_links[] = $base_url . $link;
	}
}

file_put_contents("winlinks.txt", implode("\n", $win_links));

?>
