<?php
/**
* PHP Scrape framework
* 
* set of tools and project skeleton for web site scrapping
* 
* @author Anton Shmigirilov <shmigirilov@gmail.com>
* @version 0.1
*/

/**
 * Set framework options for this session
 * @global array $GLOBALS used for options storage
 * @param array $opts associative array of options in form 'option_name' => 'option_value'
 */
function ps_set_options($opts)
{
    $GLOBALS["ps_options"] = $opts;
}

/**
 * Set default options
 * @return array associative array with set of default options
 */
function ps_default_opts()
{
    return array(
        "user_agent" => "PHP Scrape framework; Mozilla/5.0 Chrome Safari KHTML Gecko",
        "use_proxy" => false,
        "use_tidy" => false,
        "force_utf8" => false
    );
}

/**
 * Fetch document via HTTP GET request and return its body
 * @param string $url URL of requested document
 * @return string document contents
 */
function ps_url_get($url)
{
    if (isset ($GLOBALS["ps_options"]))
        $opts = $GLOBALS["ps_options"];
    else
        $opts = ps_default_opts();

    $curl = curl_init();
    if (!$curl)
        die ("curl_init() error");
    
    curl_setopt($curl, CURLOPT_USERAGENT, $opts["user_agent"]);
    
    if ($opts["use_proxy"])
    {
        curl_setopt($curl, CURLOPT_PROXY, $opts["proxy_url"]);
        curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_PROXYUSERPWD, $opts["proxy_user"] . ":" . $opts["proxy_pass"]);
        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, false);
    }

    curl_setopt($curl, CURLOPT_URL, $url);

    // follow location while HTTP redirects
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

    // empty cookiefile for transfer cookies setted by server while HTTP redirects
    curl_setopt($curl, CURLOPT_COOKIEFILE, "");
    
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $ret = curl_exec($curl);
    if (!$ret)
        die ("curl_exec() failed: " . curl_error($curl));

    curl_close($curl);

    return $ret;
}

/**
 * Download file via HTTP GET request and store it into local file
 * @param string $url URL of requested file
 * @param string $localfile path to file stored locally
 */
function ps_download($url, $localfile)
{
    if (isset ($GLOBALS["ps_options"]))
        $opts = $GLOBALS["ps_options"];
    else
        $opts = ps_default_opts();

    $curl = curl_init();
    if (!$curl)
        die ("curl_init() error");
    
    curl_setopt($curl, CURLOPT_USERAGENT, $opts["user_agent"]);
    
    if ($opts["use_proxy"])
    {
        curl_setopt($curl, CURLOPT_PROXY, $opts["proxy_url"]);
        curl_setopt($curl, CURLOPT_PROXYAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_PROXYUSERPWD, $opts["proxy_user"] . ":" . $opts["proxy_pass"]);
        curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, false);
    }

    curl_setopt($curl, CURLOPT_URL, $url);

    // follow location while HTTP redirects
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

    // empty cookiefile for transfer cookies setted by server while HTTP redirects
    curl_setopt($curl, CURLOPT_COOKIEFILE, "");
    
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	$fp = fopen($localfile, "w");
	curl_setopt($curl, CURLOPT_FILE, $fp);

    $ret = curl_exec($curl);
    if (!$ret)
        die ("curl_exec() failed: " . curl_error($curl));

    curl_close($curl);
}

/**
 * Repair broken HTML document and clean and indenting it
 * @param string $html contents of source HTML document
 * @return string resulting document
 */
function ps_html_repair($html)
{
    if (isset ($GLOBALS["ps_options"]))
        $opts = $GLOBALS["ps_options"];
    else
        $opts = ps_default_opts();

    if ($opts["use_tidy"])
    {
	    $config = array(
           'indent'          => false,
           'input-encoding'  => 'raw',
           'output-encoding' => 'raw'
	    );

	    $tidy = new tidy;
	    $tidy->parseString($html, $config);                               
	    $tidy->cleanRepair();
    
	    return tidy_get_output ($tidy);
    }
    else
    	return $html;
}

/**
 * Request HTML element using XPath
 *
 * Performs XPath query to document referred by $html 
 * and appends found nodes to $collection array 
 * where every item as associative array in form
 * 'key' => 'value', where 'key' corresponds keys of $filters,
 * 'value' is result of calling appropriate value of $filters
 *
 * @param string $html HTML document
 * @param string $xp XPath query
 * @param array $collection array of results appended to
 * @param array filters associative arrays. 
 * Keys are result string ids, values are Closures performing nodeValue transformation.
 */
function ps_query($html, $xp, &$collection, $filters)
{
    if (isset ($GLOBALS["ps_options"]))
        $opts = $GLOBALS["ps_options"];
    else
        $opts = ps_default_opts();

    $dom = new DomDocument();

    if ($opts["force_utf8"])
    	$html = '<?xml encoding="UTF-8">' . $html ;

    @$dom->loadHTML( $html );
    $xpath = new DomXPath( $dom );

    $res = $xpath->query($xp);
    foreach ($res as $obj)
    {
        $entry = array();
        
        // add default value entry
        $entry['value'] = $obj->nodeValue;

        $keys = array_keys($filters);
        foreach($keys as $key)
           $entry[$key] = $filters[$key]($obj);

        $collection[] = $entry;
    }
}

function ps_query_blocks($html, &$collection, $xp_abs, $xp_rels, $include_root = true)
{
    if (isset ($GLOBALS["ps_options"]))
        $opts = $GLOBALS["ps_options"];
    else
        $opts = ps_default_opts();

    $dom = new DomDocument();

    if ($opts["force_utf8"])
    	$html = '<?xml encoding="UTF-8">' . $html ;
    
    @$dom->loadHTML( $html );
    $xpath = new DomXPath( $dom );
        
    foreach($xp_abs as $k => $s)
    {
		$res = $xpath->query($s);
		break;
	}	

	// process root query
	foreach ($res as $obj)
	{
		$entry = [];

		// add root element value
		if ($include_root)
			$entry[$k] = $obj->nodeValue;

		// process subqueries
	 	foreach($xp_rels as $krel => $vrel)
	 	{
	 		$res_rel = $xpath->query($vrel, $obj);

	 		// add each subelements values
			$entry[$krel] = $res_rel->item(0)->nodeValue;
	 	}

		$collection[] = $entry;
	}
}

/**
 * Convert string from autodetected encoding to specified
 *
 * If option 'force_utf8' specified, no autodetection of source encoding performed
 *
 * @param string $str input string 
 * @param string $to output encoding name
 * @return string encoded string in $to encoding
 *
 */
function ps_convert_encoding($str, $to = "UTF-8")
{
    if (isset ($GLOBALS["ps_options"]))
        $opts = $GLOBALS["ps_options"];
    else
        $opts = ps_default_opts();

    $from = mb_detect_encoding($str);
    if ($opts["force_utf8"])
    	$from = "UTF-8";

	return mb_convert_encoding($str, $to, $from);
}

/**
 * Save linear array of associative arrays of data as CSV file
 *
 * @param array $data linear array of associative arrays of data
 * @param string $filename filename to store output
 * @param array $filters array of Closures performing strings transfomations
 */
function ps_out_csv($data, $filename, $filters = array(), $output_encoding = "UTF-8")
{
    global $forum_url;

    if (count($data) == 0)
    	return;

    $fp = fopen($filename, "w");
    $keys = array_keys($data[0]);
    fwrite($fp, implode("; ", $keys) . "\n");
    foreach ($data as $f)
    {
        $row = "";

        foreach ($keys as $key)
        {
            if (isset($filters[$key]))
                $row = $row . $filters[$key]($f[$key]) . "; ";
            else
                $row = $row . $f[$key] . "; ";
        }

        $row = substr($row, 0, -2) . "\n";

        $row = ps_convert_encoding($row, $output_encoding);

        fwrite($fp, $row);
    }
    
    fclose($fp);
}

?>
